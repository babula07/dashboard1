import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { CommonService } from '../common.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  public collection:any;

  constructor(private commonService:CommonService) { }

  ngOnInit(): void {
    this.commonService.getDetailList().subscribe((result)=>{
      this.collection= result;
      console.log(this.collection)

    })  
  }
  deleteDetail(id:any){
      //this.collection.splice(id)
     this.commonService.deleteDetail(id).subscribe(result=>{
     console.log("deleted");
     this.commonService.getDetailList().subscribe((result)=>{
      this.collection= result;
      console.log(this.collection)

    })  
    
    })
  }
  
   
}
