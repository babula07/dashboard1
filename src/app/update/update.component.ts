import { Component, OnInit } from '@angular/core';
import { CommonService } from '../common.service';
import { ActivatedRoute } from '@angular/router';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.css']
})
export class UpdateComponent implements OnInit {
alert:boolean =false;
EditEmployee = new FormGroup({
  Employee_Id: new FormControl(''),
  Employee_Name: new FormControl(''),
  Employee_Designation: new FormControl('')
})
  constructor(private Detail:CommonService,private router:ActivatedRoute ) { }

  ngOnInit(): void {
  this.Detail.getCurrentData(this.router.snapshot.params.id).subscribe((result)=>{
    
  })
  
  }
}
