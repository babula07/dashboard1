import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup} from '@angular/forms';
import { CommonService } from '../common.service';

@Component({
  selector: 'app-add-detail',
  templateUrl: './add-detail.component.html',
  styleUrls: ['./add-detail.component.css']
})
export class AddDetailComponent implements OnInit {
  alert:boolean =false;
  addEmployee= new FormGroup({
    Employee_Id: new FormControl(''),
    Employee_Name: new FormControl(''),
    Employee_Designation: new FormControl('')
  })
  constructor(private Detail:CommonService) { }

  ngOnInit(): void {
  }
  CreateEmployee(){
    //console.log(this.addEmployee.value);
    this.Detail.addDetail(this.addEmployee.value).subscribe((result)=>{
      this.alert = true;
      this.addEmployee.reset({});
      console.log("get data from service",result)
    })
  }

  closeAlert(){
    this.alert =false;
  }
}
