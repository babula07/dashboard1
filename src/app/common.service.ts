import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { $ } from 'protractor';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CommonService {
  URL="http://localhost:3000/Detail"

  constructor(private _http:HttpClient) { }
  getDetailList(){
    //console.log("this method works")
    return this._http.get(this.URL);
  }

   addDetail(data: any){
     return this._http.post(this.URL, data);
   }

   deleteDetail(id:any): Observable<object>{
    return this._http.delete('http://localhost:3000/Detail/' +id);
   }

   getCurrentData(id: any){
     return this._http.get(`$(this.URL)/$(id)`)
   }
}
