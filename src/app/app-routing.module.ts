import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListComponent } from './list/list.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { AddDetailComponent } from './add-detail/add-detail.component';
import { UpdateComponent } from './update/update.component';

const routes: Routes = [
  {component:LoginComponent, path:'login'},
  {component:RegisterComponent, path:'register'},
  {component:ListComponent, path:'list'},
  {component:AddDetailComponent, path:'add'},
  {component:UpdateComponent, path:'update/:id'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
